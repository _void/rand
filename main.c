#include <fcntl.h>
#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

// Flags
int iflag;       // print as integer
int nflag;       // Print without newline at the end
int bcount = 16; // amount of bytes to generate

// Functions
char *get_bytes(int fd) {
  char *temp = malloc(bcount * sizeof(char));
  if (temp == NULL) {
    perror("malloc");
    _exit(4);
  }

  if (read(fd, temp, bcount) <= 0) {
    perror("read");
    _exit(2);
  }

  return temp;
}

int main(int argc, char *argv[]) {
  int randfd = open("/dev/urandom", O_RDONLY);
  if (randfd == -1) {
    perror("open");
    _exit(3);
  }

  char ch;
  while ((ch = getopt(argc, argv, "b:in")) != -1) {
    switch (ch) {
    case 'i':
      iflag = 1;
      break;
    case 'b':
      bcount = atoi(optarg);
      break;
    case 'n':
      nflag = 1;
      break;
    case ':':
      fprintf(stderr, "Missing value");
      _exit(1);
      break;
    case '?':
      fprintf(stderr, "Unknown flag");
      _exit(1);
      break;
    }
  }
  argc -= optind;
  argv += optind;

  char *bytes = get_bytes(randfd);
  close(randfd);

  if (iflag) {
    int *integers = (int *)bytes;
    for (int i = 0; i < bcount / sizeof(int) + 1; i++)
      printf("%d", integers[i]);
  } else
    for (int i = 0; i < bcount; i++)
      printf("%c", bytes[i]);

  if (!nflag)
    printf("\n");

  free(bytes);
  return 0;
}
