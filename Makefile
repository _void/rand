OBJ := main.o
CC = gcc
CFLAGS = -c -Wall
LDFLAGS =
PROG = rand

all: $(OBJ)
	$(CC) $(OBJ) $(LDFLAGS) -o $(PROG)

main.o: ./main.c
	$(CC) $(CFLAGS) ./main.c

clean:
	rm $(PROG) *.o
