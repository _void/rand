# rand
Cli programm that print random bytes
# BUILD
```shell
make
```

# USAGE
-b : amount of bytes
-i : print integers(instead of characters)
-n : print without new line at the end
```shell
./rand -b 32 -i
```
# Why?
Just for fun
